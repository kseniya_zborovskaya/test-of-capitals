window.addEventListener("load", function () {

    var testMainDiv = document.querySelector(".test-main-div");
    var test1Btn = this.document.querySelector(".test-1-btn");
    var arrayAnswers = [];
    var arrayTrueAnswers = ['Tokio', 'London', 'Canberra', 'Sofia', 'Reykjavik', 'Wellington', 'Edinburgh', 'Kiev', 'Cardiff', 'Stockholm'];

    var createGuess = function (img, country, city1, city2, city3, city4, id) {
        return `<img class = 'test-1-img' alt = 'img-in-test' style = 'width:400px; height: 200px;' src = "${img}">
        <ul class = 'ul-in-test'>Capital of ${country} is...
        <li class = 'li-in-test' id = ${id}>${city1}</li>
        <li class = 'li-in-test' id = ${id}>${city2}</li>
        <li class = 'li-in-test' id = ${id}>${city3}</li>
        <li class = 'li-in-test' id = ${id}>${city4}</li>
        </ul>
        `
    };

    var testAnswers = function (arr1, arr2) {
        var sum = 0;
        for (var i = 0; i < arr1.length; i++) {
            if (arr1[i] === arr2[i]) {
                sum++;
            } else {

            }
        }
        return sum;
    };

    test1Btn.addEventListener('click', () => {
        testMainDiv.innerHTML = createGuess('./images/tokio.jpg', 'Japan', 'Tokio', 'Rome', 'London', 'Paris', '1');
    });

    document.addEventListener('click', (event) => {
        const elementId = event.target.getAttribute('id');
        if (elementId) {
            const elementValue = event.target.textContent;
            arrayAnswers.push(elementValue);
        }
        if (elementId === '1') {
            testMainDiv.innerHTML = createGuess('./images/header.jpg', 'the UK', 'Warsaw', 'Paris', 'Rome', 'London', '2');
        } else if (elementId === '2') {
            testMainDiv.innerHTML = createGuess('./images/canberra.jpg', 'Australia', 'Bern', 'Canberra', 'Andorra la Vella', 'Paramaribo', '3');
        } else if (elementId === '3') {
            testMainDiv.innerHTML = createGuess('./images/sofia.jpg', 'Bulgaria', 'Paramaribo', 'Canberra', 'Sofia', 'Buenos Aires', '4');
        } else if (elementId === '4') {
            testMainDiv.innerHTML = createGuess('./images/reykjavik.jpg', 'Iceland', 'Reykjavik', 'Monaco', 'Luanda', 'Minsk', '5');
        } else if (elementId === '5') {
            testMainDiv.innerHTML = createGuess('./images/wellington.jpg', 'New Zealand', 'Stockholm', 'Paris', 'Buenos Aires', 'Wellington', '6');
        } else if (elementId === '6') {
            testMainDiv.innerHTML = createGuess('./images/edinburgh.jpg', 'Scotland', 'Cardiff', 'Luanda', 'Edinburgh', 'London', '7');
        } else if (elementId === '7') {
            testMainDiv.innerHTML = createGuess('./images/kiev.jpg', 'Ukraine', 'Warsaw', 'Kiev', 'Edinburgh', 'Bern', '8');
        } else if (elementId === '8') {
            testMainDiv.innerHTML = createGuess('./images/cardiff.jpg', 'Wales', 'Cardiff', 'Wellington', 'Sofia', 'Kiev', '9');
        } else if (elementId === '9') {
            testMainDiv.innerHTML = createGuess('./images/stockholm.jpg', 'Sweden', 'Warsaw', 'Minsk', 'Monaco', 'Stockholm', '10');
        } else if (elementId === '10') {
            var result = testAnswers(arrayAnswers, arrayTrueAnswers);
            var lastImg;
            var levelName;
            if (result < 3) {
                lastImg = './images/beginner.jpg';
                levelName = "Beginner";
            } else if (result < 8) {
                lastImg = './images/good.jpg';
                levelName = "Good";
            } else {
                lastImg = './images/professor.jpg';
                levelName = "Professor";
            }
            testMainDiv.innerHTML = `
            <img class = 'test-1-img' alt = 'end-img' style = 'width: 400px; height: 200px;' src = './images/test.jpg'>
            <button class = 'the-end-button'>The End</button>
            <p>${result}/10!</p>
            <p>${levelName}</p>
            <img class = 'last-img' alt = 'level-img' style = 'width: 100px; height: 100px;' src = '${lastImg}'>
            `
        } else {

        }
    });

});